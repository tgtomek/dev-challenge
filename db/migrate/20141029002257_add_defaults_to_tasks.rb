# Update database defaults for complete and archived

class AddDefaultsToTasks < ActiveRecord::Migration
  def change
    reversible do |dir|
      dir.up do
        change_column :tasks, :complete, :boolean, default: false, null: false
        change_column :tasks, :archived, :boolean, default: false, null: false
      end
      dir.down do
        change_column :tasks, :complete, :boolean, default: nil, null: true
        change_column :tasks, :archived, :boolean, default: nil, null: true
      end
   end
  end
end
