# Comments

Feel free to leave any comments or notes for us here (or inline with code, e.g., `###COMMENT: [your comments here]`).



#### Add Google Maps integration
* For addresses at same location I offset the pins slightly, although in a real app it'd require a bit more detail.
* I used a simple setTimout to handle the google api threshold limits.
* I did not handle the case where one of the plot_me addresses returned multiple values, and the street number was not valid I believe.
* I haven't used the google maps api much so I'm sure the implementation could be improved, both with the logic and the UI.

#### Ajax: complete & archive support
* For the Archive link I used the Rails remote helper, though typically I prefer to handle all ajax calls explicitly with jQuery.
* I just prefer to keep the that logic out of the views.

#### Feedback
* If you have any feedback on the code or find any errors then please let me know. Always good to learn something.
