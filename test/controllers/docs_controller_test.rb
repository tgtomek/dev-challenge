require 'test_helper'

class DocsControllerTest < ActionController::TestCase

  test "routes to docs#readme" do
    assert_routing '/readme', controller: 'docs', action: 'readme'
  end

  test "responds ok" do
    get :readme
    assert_response :success
  end

  test "renders readme template" do
    get :readme
    assert_template :readme
  end

  test "renders tasklist.md" do
    get :readme
    assert_select 'h1', 'Task List Instructions'
  end

  test "renders comments.md" do
    get :readme
    assert_select 'h1', 'Comments'
  end

  test "renders section_E.md" do
    get :readme
    assert_select 'h1', 'Section E'
  end
end