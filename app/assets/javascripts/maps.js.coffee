# Add Google Maps integration

$(document).on 'page:load', (event) ->

  if $('#map-canvas').length

    plot_me =
      visited: [
        '5175 W San Fernando Rd, Los Angeles, CA 90039'
        '10 Congress Street #202, Pasadena, CA 91105'
        '471 W Broadway Ave. #340, Glendale, CA 91204'
        '800 S Fairmount Ave. #425,  Pasadena, CA 91105'
        '1560 Chevy Chase Dr. #325,  Glendale, CA 91206'
      ]
      revisit: [
        '729 Ivy Street, Glendale, CA 91105'
        '2329 W. Rosecrans Ave., Gardena, CA 90249'
        '10 Congress Street #300, Pasadena, CA 91105'
      ]
      unvisited: [
        '463 Riverdale Dr, Glendale, CA 91204'
        '1560 Chevy Chase Dr. #345, Glendale, CA 91206'
        '1138 N Brand Blvd #B, Glendale, CA 91202'
        '280 E Colorado Blvd #103, Pasadena, CA 91101'
        '800 S Fairmont Avenue #310, Pasadena, CA 91105'
        '1141 N Brand Blvd #306, Glendale, CA 91202'
      ]

    plotLocations(plot_me)


plotLocations = (locationObj) ->
  myMap.geocoder  = new google.maps.Geocoder()
  latlng          = new google.maps.LatLng(34.137, -118.259)
  mapOptions =
    zoom: 10
    center: latlng

  myMap.map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions)

  for locationType, locationList of locationObj
    for location in locationList
      geocode(location, locationType)


geocode = (location, locationType) ->
  myMap.geocoder.geocode
    address: location
  , plot(location, locationType)


plot = (location, locationType) ->
  geocodeCB = (results, status) ->

    if status is google.maps.GeocoderStatus.OK
      geoLocation   =  results[0].geometry.location
      lat           =  geoLocation.lat()
      lng           =  geoLocation.lng()

      for loc in myMap.locations
        hasDuplicate = loc[0] is lat and loc[1] is lng
        geoLocation  = getAlternativePinLatLng(lat, lng) if hasDuplicate

      myMap.locations.push [lat, lng]

      marker = new google.maps.Marker(
        map: myMap.map
        position: geoLocation
        icon: new google.maps.MarkerImage(myMap.pin.urlBase + myMap.pin[locationType])
      )

    else
      if status is google.maps.GeocoderStatus.OVER_QUERY_LIMIT
        setTimeout (->
          geocode(location, locationType)
        ), 2000
      else
        alert "Geocode error: " + status


getAlternativePinLatLng = (lat, lng) ->
  min = .9999991
  max = 1.000002
  offsetLat   = lat * (Math.random() * (max - min) + min)
  offsetLng   = lng * (Math.random() * (max - min) + min)
  new google.maps.LatLng(offsetLat, offsetLng)


myMap =
  map: undefined
  geocoder: undefined
  locations: []
  pin:
    urlBase: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|"
    visited: "C43D3C"
    revisit: "DE8500"
    unvisited: "217000"
