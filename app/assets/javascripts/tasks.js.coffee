$(document).on 'page:load', (event) ->
  initCompleteCheckbox()
  initArchiveLink()


# Add complete support
initCompleteCheckbox = ->
 $('#task-list').on 'click', "input[name='complete']", ->
   $checkbox      = $(this)
   originalState  = $checkbox.attr('checked') or false
   newState       = !originalState

   updateCompletedClass $checkbox, newState  # Style completed tasks

   $.ajax
     type:       "PUT"
     dataType:   "json"
     url:        $checkbox.attr('data-url')
     data:       { task: 'complete': newState }
     success: ->
       $checkbox.attr('checked', newState)
     error: (msg) ->
       $checkbox.prop('checked', originalState)
       updateCompletedClass $checkbox, originalState  # Style completed tasks
       alert "Sorry, there was an error saving the update"


# Add archive support
initArchiveLink = ->
  $("a.archive").on("ajax:success", (e, data, status, xhr) ->
    $(this).closest('.task').hide()
  ).on "ajax:error", (e, xhr, status, error) ->
    alert 'error'


# Style completed tasks
updateCompletedClass = ($item, completed) ->
  $label = $item.siblings('.label-success')

  if completed
    newHtml = '<span class="label label-success">Done</span>'
    if $label.length then $label.show() else $item.parent().append(newHtml)
  else
    $label.hide()
