module ApplicationHelper

  # Add Google Maps integration
  def maps_controller?
    controller.controller_name == 'maps'
  end

end
